<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_tour')->nullable();
            $table->dateTime('checkin_date')->nullable();
            $table->dateTime('checkout_date')->nullable();
            $table->unsignedInteger('country_id')->index();
//            $table->foreign('country_id')
//                ->references('id')
//                ->on('countries')
//                ->onDelete('cascade');
            $table->text('city')->nullable();
            $table->unsignedInteger('hotel_id')->index();
            $table->string('name_hotel')->nullable();
            $table->integer('stars_count')->nullable();
            $table->string('paxes')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->text('description')->nullable();
            $table->longText('services')->nullable();
            $table->longText('testimonials')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
