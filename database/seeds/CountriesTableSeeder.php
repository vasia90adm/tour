<?php

use Illuminate\Database\Seeder;
use \App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new Country();
        $post->country_id = 18754;
        $post->name = 'Австрия';
        $post->save();

        $post = new Country();
        $post->country_id = 20613;
        $post->name = 'ОАЭ';
        $post->save();

        $post = new Country();
        $post->country_id = 18757;
        $post->name = 'Андорра';
        $post->save();

        $post = new Country();
        $post->country_id = 18792;
        $post->name = 'Португалия';
        $post->save();

        $post = new Country();
        $post->country_id = 18764;
        $post->name = 'Болгария';
        $post->save();

        $post = new Country();
        $post->country_id = 284350;
        $post->name = 'Сейшелы';
        $post->save();

        $post = new Country();
        $post->country_id = 188015;
        $post->name = 'Венгрия';
        $post->save();

        $post = new Country();
        $post->country_id = 216683;
        $post->name = 'Словакия';
        $post->save();

        $post = new Country();
        $post->country_id = 18741;
        $post->name = 'Греция';
        $post->save();

        $post = new Country();
        $post->country_id = 288271;
        $post->name = 'Словения';
        $post->save();

        $post = new Country();
        $post->country_id = 223991;
        $post->name = 'Грузия';
        $post->save();

        $post = new Country();
        $post->country_id = 20625;
        $post->name = 'Таиланд';
        $post->save();

        $post = new Country();
        $post->country_id = 16004;
        $post->name = 'Доминиканская Республика';
        $post->save();

        $post = new Country();
        $post->country_id = 18464;
        $post->name = 'Тунис';
        $post->save();

        $post = new Country();
        $post->country_id = 18498;
        $post->name = 'Египет';
        $post->save();

        $post = new Country();
        $post->country_id = 18803;
        $post->name = 'Турция';
        $post->save();

        $post = new Country();
        $post->country_id = 17762;
        $post->name = 'Израиль';
        $post->save();

        $post = new Country();
        $post->country_id = 204426;
        $post->name = 'Финляндия';
        $post->save();

        $post = new Country();
        $post->country_id = 250722;
        $post->name = 'Индонезия';
        $post->save();

        $post = new Country();
        $post->country_id = 18808;
        $post->name = 'Франция';
        $post->save();

        $post = new Country();
        $post->country_id = 18747;
        $post->name = 'Испания';
        $post->save();

        $post = new Country();
        $post->country_id = 18810;
        $post->name = 'Хорватия';
        $post->save();

        $post = new Country();
        $post->country_id = 18770;
        $post->name = 'Италия';
        $post->save();

        $post = new Country();
        $post->country_id = 18812;
        $post->name = 'Черногория';
        $post->save();

        $post = new Country();
        $post->country_id = 18772;
        $post->name = 'Кипр';
        $post->save();

        $post = new Country();
        $post->country_id = 18813;
        $post->name = 'Чехия';
        $post->save();

        $post = new Country();
        $post->country_id = 293483;
        $post->name = 'Маврикий';
        $post->save();

        $post = new Country();
        $post->country_id = 20629;
        $post->name = 'Шри-Ланка';
        $post->save();

        $post = new Country();
        $post->country_id = 275410;
        $post->name = 'Мальдивы';
        $post->save();

        $post = new Country();
        $post->country_id = 337852;
        $post->name = 'Япония';
        $post->save();

        $post = new Country();
        $post->country_id = 341454;
        $post->name = 'Мальта';
        $post->save();
    }
}
