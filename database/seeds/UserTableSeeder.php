<?php

use Illuminate\Database\Seeder;

use \App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super = new User();
        $super->name = 'SuperAdmin';
        $super->email = 'super_admin@sm-dev.pp.ua';
        $super->password = bcrypt('sm-dev');
        $super->save();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@sm-dev.pp.ua';
        $admin->password = bcrypt('sm-dev');
        $admin->email_verified_at =
        $admin->save();

        $partner = new User();
        $partner->name = 'Partner';
        $partner->email = 'partner@sm-dev.pp.ua';
        $partner->password = bcrypt('sm-dev');
        $partner->save();
    }
}
