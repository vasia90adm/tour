<?php

namespace App\Http\Controllers;

use App\Country;
use App\Tour;
use App\TourTestimonial;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;


class TourParserController extends Controller
{
    public function parse(){

        //delete old tours
        Tour::where('checkin_date', '<=', date('Y-m-d', strtotime('+ 2 days')))->delete();


        $countries = Country::all();

        if($countries) {
            foreach ($countries as $item) {

                //params count people
                for ($people = 0; $people < 3; $people++) {
                    //params checkin date
                    for ($date = 3; $date < 6; $date++) {
                        $params[$people][$date] = [
                            'country_id' => $item->country_id,
                            'beginDate' => date('Y-m-d', strtotime('+ ' . $date . ' days')),
                            'paxes' => ['adultsCount' => 2, 'childrenCount' => $people, 'infantsCount' => 0]
                        ];
                    }
                }
                //foreach tours about people
                foreach ($params as $item22) {
                    //foreach tours about checkin date
                    foreach ($item22 as $item2) {
                        //get tours with filters
                        $tours = $this->tours($item2);
                        if ($tours) {
                            $tourIds = [];
                            foreach ($tours as $item3) {
                                $testimonial = '';
                                if($item3->hotels[0]->reviews){
                                    $testimonial = json_encode($item3->hotels[0]->reviews);
                                }
                                //todo gallery
                                $images = '';
                                if($item3->hotels[0]->photoUrls){
                                    $images = $item3->hotels[0]->photoUrls;
                                }

                                //each tour 2 pieces
                                if (isset($tourIds[$item3->id]) && count($tourIds[$item3->id]) == 2) {
                                    continue;
                                } else {
                                    //tmp var calc tours(each tour 2 pieces)
                                    $tourIds[$item3->id][] = $item3->id;
//                                    dd($item3);
                                    $obj = [
                                        'name_tour' => (isset($item3->name) ? $item3->name : null),
                                        'checkin_date' => (isset($item3->checkinDate) ? $item3->checkinDate : null),
                                        'checkout_date' => (isset($item3->checkoutDate) ? $item3->checkoutDate : null),
                                        'country_id' => $item->country_id,
                                        'city' => json_encode([(isset($item3->hotels[0]->resort) ? $item3->hotels[0]->resort : ''), (isset($item3->hotels[0]->city) ? $item3->hotels[0]->city : '')]),
                                        'hotel_id' => (isset($item3->hotels[0]->id) ? $item3->hotels[0]->id : null),
                                        'name_hotel' => (isset($item3->hotels[0]->name) ? $item3->hotels[0]->name : null),
                                        'stars_count' => (isset($item3->hotels[0]->categoryStarsCount) ? $item3->hotels[0]->categoryStarsCount : null),
                                        'paxes' => json_encode((isset($item3->paxes) ? $item3->paxes : '')),
                                        'price' => (isset($item3->price->value) ? $item3->price->value : 0),
                                        'description' => (isset($item3->hotels[0]->shortDescription) ? $item3->hotels[0]->shortDescription : null),
                                        'services' => json_encode((isset($item3->hotels[0]->services) ? $item3->hotels[0]->services : '')),
                                        'testimonials' => $testimonial,
                                    ];
                                    $tour = new Tour($obj);
                                    $tour->save();
                                }
                            }
                        }
                    }
                }
//                dd($item->country_id);
            }
        }
//        dd($countries);
    }


    public function tours($params){
        $getToken = $this->token();

        $array = [
            'json' => [
                'clientId'     => 'b2c:ua',
                'departureTownId' => 1,
                'arrivalCountryId'  => $params['country_id'],
                'arrivalRegionIds'  => [],
                'arrivalTownIds'  => [],
                'childrenAges'  => [],
                'paxes'  => $params['paxes'],
                'checkinBeginDate' => $params['beginDate'],
                'checkinEndDate' => $params['beginDate'],
                'maxNightsCount' => 14,
                'minNightsCount' => 7,
                'pagingOptions' => [
                    'pageNumber' => 1,
                    'pageSize' => 1000,
                ],
                'cost' => [
                    'currencyId' => 2,
                ],
                'groupingOptions' => [
                    'type' => 1
                ],
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$getToken,
            ]
        ];

        $url = 'https://www.tui.ua/api/tour/search';

        $client = new Client();
        $response = $client->post($url, $array, ['http_errors' => true]);
        $object = json_decode(''.$response->getBody());
        if(isset($object->data)){
            return $object->data->tours;
        }else{
            return false;
        }

    }

    public function token(){
        $cookieJar = new CookieJar();

        $client = new Client([
            'cookies' => $cookieJar,
            'allow_redirects' => true,
            'decode_content' => true
        ]);

        $res = $client->get('https://www.tui.ua/');
        $header = $res->getHeaders();
        $return = str_replace('__Tui_Authorization_Identifier__=', '', urldecode($header['Set-Cookie'][0]));
        $return = trim((explode('expires=', $return))[0], '; ');
        $return = json_decode($return);

        return $return->access_token;
    }
}
