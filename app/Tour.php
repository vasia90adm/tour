<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $fillable = [
        'name_tour', 'checkin_date', 'checkout_date', 'country_id', 'city', 'hotel_id', 'name_hotel', 'stars_count', 'paxes', 'price', 'description', 'services', 'testimonials'
    ];
}
